FROM ubuntu:18.04
MAINTAINER Karim DRIDI <karim.dridi@epitech.eu>

RUN export DEBIAN_FRONTEND=noninteractive && \
	apt-get update && \
	apt-get install -y apt-utils && \
	apt-get upgrade -y && \
	apt-get install -y apt-file && \
	apt-file update

RUN export DEBIAN_FRONTEND=noninteractive && \
	apt-get install -y texlive-latex-base

RUN export DEBIAN_FRONTEND=noninteractive && \
	apt-get install -y context texlive texlive-base texlive-bibtex-extra texlive-binaries texlive-extra-utils texlive-font-utils texlive-fonts-extra texlive-fonts-extra-doc texlive-fonts-extra-links texlive-fonts-recommended texlive-fonts-recommended-doc texlive-formats-extra texlive-full texlive-games texlive-generic-extra texlive-generic-recommended texlive-htmlxml texlive-humanities texlive-humanities-doc texlive-lang-african texlive-lang-all texlive-lang-arabic texlive-lang-chinese texlive-lang-cjk texlive-lang-cyrillic texlive-lang-czechslovak texlive-lang-english texlive-lang-european texlive-lang-french texlive-lang-german texlive-lang-greek texlive-lang-indic texlive-lang-italian texlive-lang-japanese texlive-lang-korean texlive-lang-other texlive-lang-polish texlive-lang-portuguese texlive-lang-spanish texlive-latex-base texlive-latex-base-doc texlive-latex-extra texlive-latex-extra-doc texlive-latex-recommended texlive-latex-recommended-doc texlive-luatex texlive-metapost texlive-metapost-doc texlive-music texlive-omega texlive-pictures texlive-pictures-doc texlive-plain-extra texlive-plain-generic texlive-pstricks texlive-pstricks-doc texlive-publishers texlive-publishers-doc texlive-science texlive-science-doc texlive-xetex wget

RUN	wget -c "https://github.com/jgm/pandoc/releases/download/2.2.2/pandoc-2.2.2-1-amd64.deb"

RUN export DEBIAN_FRONTEND=noninteractive && \
	dpkg -i pandoc-2.2.2-1-amd64.deb && \
	rm pandoc-2.2.2-1-amd64.deb

RUN export DEBIAN_FRONTEND=noninteractive && \
	apt-get install -y gpp

RUN export DEBIAN_FRONTEND=noninteractive && \
	apt-get install -y vim nano

RUN useradd -b /home -m -s /bin/bash -U template

WORKDIR /home/template

ADD . .

RUN chown -R template:template /home/template

USER template

CMD [ "bash" ]