#!/usr/bin/env bash

show_usage() {
	echo "$(basename $0) [document.md]"
	exit 84
}

md_file="${1}"

if [[ "${md_file}" == "" ]]; then
	show_usage
fi

if [[ ! -f "${md_file}" ]]; then
	show_usage
fi

cd "$(dirname "${md_file}")"
md_dir="$(pwd)"
md_name="$(basename "${md_file}")"
cd - > /dev/null

docker container run --rm -v "`pwd`:/home/template/workdir" -w /home/template/workdir dockermd bash -c "./Epitech_md-compilation.sh ${md_name}"

