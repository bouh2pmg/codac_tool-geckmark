# Geckmark
### Docker image to get PDF from markdown

#### Installation
First of all, please login with your gitlab account :
```bash
docker login registry.gitlab.com
```

Now you are able to pull the image :
```bash
docker pull registry.gitlab.com/geckos/geckmark
```
That's all !

#### Usage
##### 1) Simple usage
```bash
docker container run --rm -v "`pwd`:/home/template/workdir" dockermd bash -c "./Epitech_md-compilation.sh workdir/[file.md]"```
```
##### 2) Use an alias
```bash
md_to_pdf () {
docker container run --rm -v "`pwd`:/home/template/workdir" dockermd bash -c "./Epitech_md-compilation.sh workdir/$1" }
```
Then just use the bash function as:
```bash
md_to_pdf [file.md]
```